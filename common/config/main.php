<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => array_merge([
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'image' => [
            'class' => 'yii\image\ImageDriver',
            'driver' => 'GD',  //GD or Imagick
        ],
        'i18n' => [
            'translations' => YII_DEBUG ? [
                /* We only need this in config for gii to work properly */
                'shop*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/modules/shop/messages',
                    'fileMap' => [
                        'shop' => 'shop.php',
                    ],
                ],
            ] : [],
        ],
    ], YII_IS_WEBAPP ? [  
        'assetManager' => [
            'linkAssets' => true,
        ],
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false,
            
            'rules' => [
                
                '<module>/<controller>/<id:\d+>'            => '<module>/<controller>/view',
                '<module>/<controller>/<action>/<id:\d+>'   => '<module>/<controller>/<action>',
                '<module>/<controller>/<action>'            => '<module>/<controller>/<action>',
                
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                
                '' => 'site/index',
            ],
        ],
    ] : []),
    
    'modules' => [
        'shop' => [
            'class' => 'common\modules\shop\Module',
        ]
    ],
];
