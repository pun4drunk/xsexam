<?php

namespace common\modules\shop\models;
use yii\db\ActiveRecord;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "shop_product".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $price
 * @property string $created_at
 * @property string $updated_at
 *
 * @property ShopCategory $category
 */
class Product extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'price'], 'required'],
            [['category_id'], 'integer'],
            [['price'], 'number'],
            [['image'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('shop', 'ID'),
            'category_id' => Yii::t('shop', 'Category'),
            'price' => Yii::t('shop', 'Price'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @inheritdoc
     * @return ProductQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductQuery(get_called_class());
    }
    
    public function getImageUrl() {
        return strlen($this->image) ? Yii::$app->getModule('shop')->imagesUrl.'/'.$this->image : false;
    }
    
    public function getCategoryOptions() {
        return ArrayHelper::map(Category::find()->all(), 'id', 'title'); 
    }
}
