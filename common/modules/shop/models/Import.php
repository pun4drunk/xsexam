<?php

/*
 * Copyright (C) 2015 Vladislav Waxme
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Description of Import
 *
 * @author Vladislav
 */
namespace common\modules\shop\models;
use Yii;

class Import extends yii\base\Model {
    
    public $file;
    public $filename;
    
    public function rules() {
        return [
            [['file'], 'file', 'skipOnEmpty' => false, 
                //'extensions' => ['csv'],
            ],
        ];
    }
    
    public function attributeLabels() {
        return [
            'file' => Yii::t('shop', 'File'),
        ];
    }
    
    public function attributeHints() {
        return [
            'file' => Yii::t('shop', 'Please upload csv file to import products from'),
        ];
    }
    
    public function save()
    {
        if ($this->validate()) {
            $userId = Yii::$app->user->id;
            $filename = Yii::$app->getModule('shop')->runtimeDir.'/'.md5($userId.  microtime()).'.csv';
            
            $this->file->saveAs($filename);
            $this->filename = $filename;
            return true;
        } else {
            return false;
        }
    }
    
}
