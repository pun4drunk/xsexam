<?php

/*
 * Copyright (C) 2015 Vladislav Waxme
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace common\modules\shop\components\helpers;
use Yii;

/**
 * Description of ImportHelper
 *
 * @author Vladislav
 */
abstract class ImportHelper {
    
    const IMAGE_PNG = 'png';
    const IMAGE_JPG = 'jpg';
    const IMAGE_JPEG = 'jpeg';
    
    public static function getData($filename, $processImages = true) {
        $data = [];
        $handle = fopen($filename, "r");
        while (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {
            list($name, $price, $image) = static::transformImportRow($row, $processImages);
            $data[] = [$name, $price, $image];
        }
        fclose($handle);
        return $data;
    }
    
    protected static function transformImportRow(array $row, $processImages) {
        
        list($name, $price, $image) = $row;
        
        $name = trim(iconv('WINDOWS-1255', 'UTF-8', $name));
        $price = preg_replace("/[^0-9\.]+/", "", $price);
        $image = static::handleImage($image, $processImages);
        
        return [$name, $price, $image];
        
    }
    
    protected static function handleImage($url, $processImages) {
        
        $result = false;
        
        if ($url && $image = file_get_contents($url)) {
            
            try {
                
                $imagesDir = Yii::getAlias('common').'/data/shop/images';

                $basename = basename($url);
                list($name, $ext) = explode(".", $basename);

                switch ($ext) {

                    case static::IMAGE_JPEG:
                    case static::IMAGE_JPG:
                        $filename = "$imagesDir/$basename";
                        if ($processImages) {
                            file_put_contents($filename, $image);
                        }
                        break;

                    case static::IMAGE_PNG:
                        $filename = "$imagesDir/$name.".static::IMAGE_JPG;
                        if ($processImages) {
                            file_put_contents($filename, $image);
                            $imageObject = imagecreatefrompng($filename);
                            imagejpeg($imageObject, $filename);
                        }
                        break;

                    default:
                        //throw new \yii\web\ServerErrorHttpException("Unsupported image type: $ext");
                        
                        //or use user-friendly way to handle unsupported
                        return false;
                }    
                
                //we don't need original image anymore, so let's unset it
                unset($image);

                if ($processImages) {
                    $width  = Yii::$app->getModule('shop')->params['image_width'];
                    $height = Yii::$app->getModule('shop')->params['image_height'];
                    $background = Yii::$app->getModule('shop')->params['image_background'];

                    $image = Yii::$app->image->load($filename);
                    //image manipulations

                    $image->resize($width, $height, \yii\image\drivers\Image::ADAPT)->background($background);
                    file_put_contents($filename, $image->render());
                    unset($image);
                }
                
                $result = basename($filename);  
                
            } catch (\Exception $e) {
                Yii::error($e->getMessage());
                $result = false;
            }
            
        }
        
        return $result;
    }
    
}
