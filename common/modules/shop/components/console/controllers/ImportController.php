<?php

/*
 * Copyright (C) 2015 Vladislav Waxme
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace common\modules\shop\components\console\controllers;
use yii\console\Controller;
use common\modules\shop\components\helpers\ImportHelper;
use Yii;

/**
 * Description of ImportController
 *
 * @author Vladislav
 */
class ImportController extends Controller {

    public function actionIndex($filename) {
        if (file_exists($filename)) {
            $data = ImportHelper::getData($filename, false);
            Yii::$app->getModule('shop')->import($data);
            unlink($filename);
        }
        return static::EXIT_CODE_NORMAL;
    }

}
