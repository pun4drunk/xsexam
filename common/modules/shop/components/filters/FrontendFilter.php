<?php

/* 
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace common\modules\shop\components\filters;

/**
 * FrontendFilter is used to restrict access to admin controller in frontend
 */
class FrontendFilter extends \yii\base\ActionFilter
{
    public $publicControllers = ['catalog'];
    /**
     * @param \yii\base\Action $action
     */
    public function beforeAction($action)
    {
        if (!in_array($action->controller->id, $this->publicControllers)) {
            throw new \yii\web\NotFoundHttpException('Not found');
        }
        
        return true;
    }
}