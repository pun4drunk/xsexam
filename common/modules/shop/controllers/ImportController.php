<?php

namespace common\modules\shop\controllers;

use Yii;
use common\modules\shop\models\Import;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use vova07\console\ConsoleRunner;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ImportController extends Controller
{
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    
                ],
            ],
        ];
    }
    
    public function actionIndex() {
        
        $model = new Import();

        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->save()) {
                Yii::$app->consoleRunner->run("shopimport/index $model->filename");
                $launched = true;
            }
        } 
            
        return $this->render('index', [
            'model' => $model,
            'launched' => $launched,
        ]);
        
    }

}
