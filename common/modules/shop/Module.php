<?php

namespace common\modules\shop;
use common\modules\shop\models\Category;
use common\modules\shop\models\Product;
use Yii;

class Module extends \yii\base\Module
{
    
    public $params = [
        'image_width' => 150,
        'image_height' => 150,
        'image_background' => '#fff',
    ];
    
    public $controllerNamespace = 'common\modules\shop\controllers';

    public $imagesDir;
    public $imagesUrl;
    
    public $runtimeDir;
    
    public function init()
    {
        parent::init();
        // custom initialization code goes here
        
        if (is_null($this->imagesDir)) {
            
            if (!Yii::getAlias('@root')) {
                Yii::setAlias('@root', dirname(Yii::getAlias('@common')));
            }
        
            $this->imagesDir = Yii::getAlias('@root').'/images/'.$this->id;
        }
        if (is_null($this->imagesUrl)) {
            $this->imagesUrl = Yii::$app->params['baseFeUrl'].'/images/'.$this->id;
        }
        
        if (is_null($this->runtimeDir)) {
            $this->runtimeDir = Yii::getAlias('@console').'/runtime/'.$this->id;
        }
        
    }
    
    public function import(array $data) {
        
        foreach ($data as $key => $row) {
            
            //import category
            list($name, $price, $image) = $row;
            if (!$name) {
                //skip empty categories
                continue;
            }
            
            $categoryAttrs = ['title' => $name];
            if (!$category = Category::findOne($categoryAttrs)) {
                $category = new Category;
                $category->attributes = $categoryAttrs;
                $category->save();
            }
            
            if (!$price) {
                //skip products without price
                continue;
            }
            
            $product = new Product;
            $product->attributes = [
                'price' => $price,
                'category_id' => $category->primaryKey,
                'image' => $image,
            ];
            
            $product->save();
            
        }
        return true;
    }
    
    public function purge() {
        Product::deleteAll();
        Category::deleteAll();
    }
}
