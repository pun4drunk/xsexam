<?php

use yii\db\Schema;
use yii\db\Migration;
use yii\helpers\BaseFileHelper;

class m150711_121757_initData extends Migration
{
    public function up()
    {
        //create data directories
        foreach ($this->getDataDirectories() as $dir) {
            if (!file_exists($dir)) {
                BaseFileHelper::createDirectory($dir);
            }    
        }
    }

    public function down()
    {
        foreach (array_reverse($this->getDataDirectories()) as $dir) {
            BaseFileHelper::removeDirectory($dir); 
        }
        return true;
    }
    
    protected function getDataDirectories() {
        $module = Yii::$app->getModule('shop');
        return [
            $module->imagesDir,
            $module->runtimeDir,
        ];
    }
    
    
}
