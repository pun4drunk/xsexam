<?php

use yii\db\Schema;
use yii\db\Migration;

class m150711_123040_initCategories extends Migration
{
    public $tableName = '{{%shop_category}}';
    
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable($this->tableName, [
            'id'        => Schema::TYPE_PK,
            'title'     => Schema::TYPE_STRING . '(255) NOT NULL',
        ], $tableOptions);
        
        $this->createIndex('category_title_idx', $this->tableName, 'title', true);
        
    }

    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
    
}
