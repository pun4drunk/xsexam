<?php

use yii\db\Schema;
use yii\db\Migration;
use common\modules\shop\components\helpers\ImportHelper;
use common\modules\shop\models\Category;
use common\modules\shop\models\Product;

class m150711_140529_importProducts extends Migration
{
    public function up()
    {
        $filename = dirname(__FILE__).'/../resources/Exam.csv';
        $data = ImportHelper::getData($filename, false);
        Yii::$app->getModule('shop')->import($data);
        return true;
    }

    public function down()
    {
        Yii::$app->getModule('shop')->purge();
    }
    
    
}
