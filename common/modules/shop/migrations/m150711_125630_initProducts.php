<?php

use yii\db\Schema;
use yii\db\Migration;

class m150711_125630_initProducts extends Migration
{
    public $tableName = '{{%shop_product}}';
    
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable($this->tableName, [
            'id'        => Schema::TYPE_PK,
            'category_id' => Schema::TYPE_INTEGER. '(11) NOT NULL',
            'price'     => Schema::TYPE_DECIMAL . '(10,2) NOT NULL',
            'image'     => Schema::TYPE_STRING . '(1000) NOT NULL',
        ], $tableOptions);
        
        $this->createIndex('product_price_idx', $this->tableName, 'price', '');
        $this->addForeignKey('product_category_fk', $this->tableName, 'category_id', '{{%shop_category}}', 'id');
        
    }

    public function safeDown()
    {
        $this->dropForeignKey('product_category_fk', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
