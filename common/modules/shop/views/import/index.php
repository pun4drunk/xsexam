<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\shop\models\Product */

$this->title = Yii::t('shop', 'Import Products');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if ($launched): ?>
        <p><?php echo Html::encode(Yii::t('shop', 'Import Launched Successfully'));?></p>
        <?php echo Html::a(Yii::t('shop', "Upload More"), \yii\helpers\Url::to('/shop/import/index'));?>
    <?php else: ?>
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    <?php endif;?>

</div>
