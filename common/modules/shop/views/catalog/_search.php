<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\shop\models\ProductSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-search">

    <?php $form = ActiveForm::begin([
        'action' => "",
        'method' => 'get',
        'options' => [
            'name' => 'productSearchForm',
        ],
    ]); ?>

    <?= $form->field($model, 'category_id')->dropDownList($model->categoryOptions, [
        'class' => 'form-control',
        'prompt' => Yii::t('shop', 'All Categories'),
        'onchange' => 'document.forms.productSearchForm.submit()',
    ]) ?>

<!--
    <div class="form-group">
        <?= Html::submitButton(Yii::t('shop', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('shop', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>
-->

    <?php ActiveForm::end(); ?>

</div>
