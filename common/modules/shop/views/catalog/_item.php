<?php

/* 
 * Copyright (C) 2015 Vladislav Waxme
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use yii\helpers\Html;

if (!$src = $model->imageUrl) {
    $src = "http://placehold.it/150x150";
}

?>
<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
    <div class="thumbnail">
      <img src="<?=$src?>" alt="<?php echo $model->category->title; ?>">
      <div class="caption text-center">
        <h3><?php echo $model->category->title;?></h3>
        <p>₪&nbsp;<?php echo sprintf("%d", $model->price)?></p>
        <p>
            <a href="#" class="btn btn-primary" role="button">
                <?php echo Html::encode(Yii::t('shop', 'Add to cart'));?>
            </a> 
        </p>
      </div>
    </div>
</div>
