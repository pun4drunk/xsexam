<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\shop\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('shop', 'Products');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php \yii\widgets\Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'image' => [
                'label' => $searchModel->getAttributeLabel('image'),
                'value' => function($data){
                    $result = "";
                    if ($src = $data->imageUrl) {
                       $result = Html::img($src);
                    }
                    return $result;
                },
                'format' => 'raw',
                'options' => [
                    'width' => '150px',
                ]
            ],
            'category' => [
                'filter' => Html::activeDropDownList($searchModel, 'category_id', $searchModel->categoryOptions, [
                    'class' => 'form-control',
                    'prompt' => Yii::t('shop', 'All Categories'),
                ]),
                'label' => $searchModel->getAttributeLabel('category_id'),
                'value' => 'category.title'
            ],
            'price' => [
                'filter' => false,
                'label' => $searchModel->getAttributeLabel('price'),
                'value' => function($data){
                    return "₪ $data->price";
                },
            ],
            
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php \yii\widgets\Pjax::end(); ?>

</div>
