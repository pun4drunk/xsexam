XSitesExam: demo e-commerce solution based on Yii2
===============================

OVERVIEW
-------------------

This project is built upon Yii2 Advanced Application Skeleton.
The template includes three tiers: front end, back end, and console, each of which
is a separate Yii application. The template is designed to work in a team development environment. It supports
deploying the application in different environments.

Custom part encapsulates e-shop logic in Yii2 module, to be reused later in any Yii2 Application easily.
3 tier application is chosen with the following reasons:

* **we need backend for e-shop module**
* **in order to support file import (which can be a long operation) we need a console application**

demo application is available within following domains

##[Frontend: http://xsexam.magic-by.com](http://xsexam.magic-by.com)

Credentials:

* **login:** user
* **password:** userpass123

Login allows you to visit product catalog [http://xsexam.magic-by.com/shop/catalog](http://xsexam.magic-by.com/shop/catalog)


##[Backend: http://admin.xsexam.magic-by.com](http://admin.xsexam.magic-by.com)

Credentials:

* **login:** admin
* **password:** adminpass123

Login makes you possible to 

* **manage products** - with [http://admin.xsexam.magic-by.com/shop/product](http://admin.xsexam.magic-by.com/shop/product)
* **import products** - with [http://admin.xsexam.magic-by.com/shop/import](http://admin.xsexam.magic-by.com/shop/import)

STEP BY STEP DESCRIPTION
-------------------
### Generated module code
Great Gii code generation tool is used to create directory structure of Shop module
```
common/
    modules/
        shop/
            controllers/
                DefaultController.php
            views/
                default/
                    index.php
            Module.php
```

### Build database
Out-of-the-box Yii2 migration API is used to create database schema in object-oriented way
```
common/
    modules/
        shop/
            migrations/
                m150711_121757_initData.php
                m150711_123040_initCategories.php
                m150711_125630_initProducts.php
```
* **initData** - create working directories (for product images and imported files)
* **initCategories** - create categories table
* **initProducts** - create products table with categories table relation

### Create models
Base category and product functionality is generated with Gii tool. 
i18n support is included for model attribute labels.
```
common/
    modules/
        shop/
            models/
                Category.php
                CategoryQuery.php
                Product.php
                ProductQuery.php
```

### Import demo data
Initial import is written as a migration but uses the functionality that will be later implemented to support file upload with web interface
```
common/
    modules/
        shop/
            components/
                helpers/
                    ImportHelper.php
            migrations/
                m150711_140529_importProducts.php
            Module.php
```
The following issues are resolved within file import (based on provided example)

* **convert character encoding from WIN-1255 to UTF-8**
* **handle rows with no image url provided, or invalid image url (like http://www.xsites.co.il/biz/exam/index.php)**
* **resize images with specified attributes (applied with module configuration) using GD driver and yii2-image extension**


### Create pages
Base product pages functionality is generated with Gii tool. 
Then the pages are customized to suit the desired functionality.

```
common/
    modules/
        shop/
            controllers/
                CatalogController.php
                ProductController.php
            models/
                Product.php
            views/
                catalog/
                product/
```

### Restrict Access
FrontendFilter is created and attached with module configuration in order to limit access to module routes on frontend
```
common/
    modules/
        shop/
            components/
                filters/
                    FrontendFilter.php
frontend/
    config/
        main.php
```

### Implement web import support
To support web file import, we need the following:

* **Create console controller** - we implement console controller to use file import functionality, just like data import migration
```
common/
    modules/
        shop/
            components/
                console/
                    controllers/
                        ImportController.php
```

* **Create web page** - we create MVC for web file import, that will invoke console command in order to actually import data we need.

```
common/
    modules/
        shop/
            components/
                console/
                    controllers/
                        ImportController.php
                    models/
                        Import.php
                    views/
                        import/
                            _form.php
                            index.php
```

### Сonclusion
Next steps of this project would be:

* **implementing cart functionality**
* **i18n: translations to desired languages**
* **design customizings**
* **security improvements**